const urlPerguntaApi = "http://10.162.111.95:8080/questao";
const urlPlacarApi = "http://10.162.110.214:8080/pontuacao";
const urlJogoApi = "http://10.162.110.214:8080/jogo";
const divPergunta = document.querySelector("#pergunta");
const divRspostas = document.querySelector("#respostas");
const divPlacar = document.querySelector("#placar");
const idJogo = 2;
const idJogador = 1;
const idPergunta = 2;

function enviaResposta(){
    let resposta = new Object();

    resposta.idJogo = idJogo;
    resposta.idJogador = idJogador;
    resposta.idPergunta = idPergunta;
    resposta.idResposta = verificaRespostaSelecionada();
    
    fetch(urlJogoApi, 
        {
            method: 'PUT',  
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(resposta)
        })
    .then(extrairJSON)
    .then(respostaRespostaJogo);    
}

function respostaRespostaJogo(){
    
}


function preparaPlacar(){
    fetch(urlPlacarApi + "/" + idJogo + "/" + idJogador)
    .then(extrairJSON)
    .then(carregaPlacar);

}

function carregaPlacar(placar){
    divPlacar.innerHTML = placar.pontuacao;
}

function preparaPergunta(idPergunta){

    fetch(urlPerguntaApi + "/" + idPergunta)
    .then(extrairJSON)
    .then(carregaPergunta);

    fetch(urlPerguntaApi + "/" + idPergunta)
    .then(extrairJSON)
    .then(carregaResposta);
}

function carregaPergunta(pergunta){
    divPergunta.innerHTML = pergunta.pergunta;
}


function carregaResposta(listaResposta){
    
    listaResposta.respostas.forEach(resposta => {
        let label = document.createElement("label");
        let radioResposta = document.createElement("input");
        radioResposta.type = "radio";
        radioResposta.id = resposta.id;
        label.innerText = resposta.descricao;
        divRspostas.appendChild(radioResposta);
        divRspostas.appendChild(label);
    });
}

function extrairJSON(resposta){
    return resposta.json();
}

function verificaRespostaSelecionada(){
    let inputs = document.querySelectorAll("input");
    for (const input of inputs) {
        if(input.type === "radio" && input.checked)
            return input.id;
    }
}

preparaPergunta(idPergunta);
preparaPlacar();
