const urlJogoApi = "http://localhost:8082/jogo";
const btnJogo = document.querySelector("#btnJogo");
const txtNome = document.querySelector("#txtNome");


function preparaNovoJogo(){
    let novoJogo = new Object();
    novoJogo.idJogador = 1;
    novoJogo.numeroQuestoes = 10;
    novoJogo.questaoAtual = 0;
    novoJogo.acertos = 0;
    novoJogo.erros = 0;

    fetch(urlJogoApi, 
            {
                method: 'POST',  
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(novoJogo)
            })
    .then(extrairJSON)
    .then(respostaNovoJogo);
}

function respostaNovoJogo(data){

}

function extrairJSON(resposta){
    return resposta.json();
}

btnJogo.onclick = preparaNovoJogo;
